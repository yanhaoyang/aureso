require 'rails_helper'

RSpec.describe ModelType, type: :model do
  it "must have car_model, name, slug and base_price" do
    mt = ModelType.new
    expect(mt.save).to be false
    %i{ car_model name slug base_price }.each do |attr|
      expect(mt.errors[attr]).to include("can't be blank")
    end
    expect(mt.errors[:base_price]).to include("is not a number")
  end
end
