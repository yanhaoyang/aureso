require 'rails_helper'

RSpec.describe CarModel, type: :model do
  it "must have organization, name and slug" do
    m = CarModel.new
    expect(m.save).to be false
    %i{ organization name slug }.each do |attr|
      expect(m.errors[attr]).to include("can't be blank")
    end
  end
end
