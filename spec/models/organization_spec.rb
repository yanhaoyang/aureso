require 'rails_helper'

RSpec.describe Organization, type: :model do
  it "must have type, name, public_name and pricing_policy" do
    o = Organization.new
    expect(o.save).to be false
    %i{ type name public_name pricing_policy }.each do |attr|
      expect(o.errors[attr]).to include("can't be blank")
    end
  end
end
