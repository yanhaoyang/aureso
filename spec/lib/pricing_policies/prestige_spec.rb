require 'spec_helper'
require 'pricing_policies'

describe PricingPolicies::Prestige do
  it "fetches data from remote and calculates margin" do
    obj = double("pricing_policy")
    allow(obj).to receive(:name) { "Prestige" }
    allow(obj).to receive(:http_expires) { nil }
    allow(obj).to receive(:http_last_modified) { Time.now - 3000 }
    allow(obj).to receive(:http_etag) { '"f82b13111731a927a0ed726f86a4fb6e"' }
    expect(obj).to receive(:http_last_modified=).with(Time.parse("2016-07-01 03:56:21 +0100"))
    expect(obj).to receive(:http_etag=).with('"06d3b0ce9eef3bf0940965f09e0c083c"')
    expect(obj).to receive(:margin=).with(51)

    VCR.use_cassette("pricing_policies-prestige") do
      PricingPolicies.find(obj).refresh
    end
  end
end
