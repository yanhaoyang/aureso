require 'spec_helper'
require 'pricing_policies'

describe PricingPolicies::Flexible do
  it "fetches data from remote and calculates margin" do
    obj = double("pricing_policy")
    allow(obj).to receive(:name) { "Flexible" }
    allow(obj).to receive(:http_expires) { Time.now - 300 }
    allow(obj).to receive(:http_last_modified) { Time.now - 3000 }
    allow(obj).to receive(:http_etag) { '"f82b13111731a927a0ed726f86a4fb6e"' }
    expect(obj).to receive(:http_expires=).with(Time.parse("2016-07-01 03:10:31 +0000"))
    expect(obj).to receive(:margin=).with(473)

    VCR.use_cassette("pricing_policies-flexible") do
      PricingPolicies.find(obj).refresh
    end
  end
end
