require 'spec_helper'
require 'pricing_policies'

describe PricingPolicies::Fixed do
  it "fetches data from remote and calculates margin" do
    obj = double("pricing_policy")
    allow(obj).to receive(:name) { "Fixed" }
    allow(obj).to receive(:http_expires) { Time.now - 300 }
    allow(obj).to receive(:http_last_modified) { Time.now - 3000 }
    allow(obj).to receive(:http_etag) { '"f82b13111731a927a0ed726f86a4fb6e"' }
    expect(obj).to receive(:http_expires=).with(Time.parse("2016-07-01 00:54:22 +0000"))
    expect(obj).to receive(:http_last_modified=).with(Time.parse("2016-06-30 19:36:11 +0000"))
    expect(obj).to receive(:margin=).with(15)

    VCR.use_cassette("pricing_policies-fixed") do
      PricingPolicies.find(obj).refresh
    end
  end
end
