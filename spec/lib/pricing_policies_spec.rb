require 'spec_helper'
require 'pricing_policies'

describe PricingPolicies do
  it "finds a policy implementation by name" do
    obj = double("pricing_policy")
    allow(obj).to receive(:name) { "Fixed" }
    expect(PricingPolicies.find(obj).class.name).to eq("PricingPolicies::Fixed")
  end

  %w{ Flexible Fixed Prestige }.each do |policy|
    it "changes nothing when http status is 304 (#{policy})" do
      allow(RestClient::Request).to receive(:execute) { raise RestClient::NotModified }

      obj = double("pricing_policy")
      allow(obj).to receive(:name) { policy }
      allow(obj).to receive(:http_expires) { Time.now - 300 }
      allow(obj).to receive(:http_last_modified) { Time.now - 3000 }
      allow(obj).to receive(:http_etag) { '"f82b13111731a927a0ed726f86a4fb6e"' }

      PricingPolicies.find(obj).refresh
    end

    it "raises an exception when http status is not 200 (#{policy})" do
      allow(RestClient::Request).to receive(:execute) { raise RestClient::ResourceNotFound }

      obj = double("pricing_policy")
      allow(obj).to receive(:name) { policy }
      allow(obj).to receive(:http_expires) { Time.now - 300 }
      allow(obj).to receive(:http_last_modified) { Time.now - 3000 }
      allow(obj).to receive(:http_etag) { '"f82b13111731a927a0ed726f86a4fb6e"' }

      expect { PricingPolicies.find(obj).refresh }.to raise_error(RestClient::ResourceNotFound)
    end

    it "does not update margin before it expires (#{policy})" do
      obj = double("pricing_policy")
      allow(obj).to receive(:name) { policy }
      allow(obj).to receive(:http_expires) { Time.now + 300 }

      VCR.use_cassette("pricing_policies-#{policy.downcase}") do
        PricingPolicies.find(obj).refresh
      end
    end
  end
end
