require "rails_helper"

RSpec.describe "api/v1/model_types", type: :request do
  let(:model_type) { FactoryGirl.create :model_type }

  it "returns 401 when the request does not have API key" do
    post "/api/v1/models/#{model_type.car_model.slug}/model_types_price/#{model_type.slug}"
    expect(response.status).to eq 401
  end

  it "returns 503 when it cannot refresh pricing policy" do
    allow(RestClient::Request).to receive(:execute) { raise RestClient::ResourceNotFound }

    data = {
      base_price: 998800,
    }
    headers = {
      "api-key": model_type.car_model.organization.api_key,
    }
    post "/api/v1/models/#{model_type.car_model.slug}/model_types_price/#{model_type.slug}",
      data, headers

    expect(response.status).to eq 503
  end

  it "returns a list of model_types in an Array" do
    data = {
      base_price: 998800,
    }
    headers = {
      "api-key": model_type.car_model.organization.api_key,
    }
    VCR.use_cassette("pricing_policies-fixed") do
      post "/api/v1/models/#{model_type.car_model.slug}/model_types_price/#{model_type.slug}",
        data, headers
    end

    expect(response.status).to eq 200

    data = JSON.parse(response.body)
    expect(data["model_type"]["name"]).to eq("bmw_116i")
    expect(data["model_type"]["base_price"]).to eq(998800)
    expect(data["model_type"]["total_price"]).to eq(998815)
  end
end
