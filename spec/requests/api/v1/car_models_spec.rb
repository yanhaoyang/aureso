require "rails_helper"

RSpec.describe "api/v1/car_models", type: :request do
  let(:model_type) { FactoryGirl.create :model_type }

  it "returns 401 when the request does not have API key" do
    get "/api/v1/models/#{model_type.car_model.slug}/model_types"
    expect(response.status).to eq 401
  end

  it "returns 503 when it cannot refresh pricing policy" do
    allow(RestClient::Request).to receive(:execute) { raise RestClient::ResourceNotFound }

    api_key = model_type.car_model.organization.api_key

    get "/api/v1/models/#{model_type.car_model.slug}/model_types", nil,
      "api-key" => api_key

    expect(response.status).to eq 503
  end

  it "returns a list of model_types in an Array" do
    api_key = model_type.car_model.organization.api_key

    VCR.use_cassette("pricing_policies-fixed") do
      get "/api/v1/models/#{model_type.car_model.slug}/model_types", nil,
        "api-key" => api_key
    end

    expect(response.status).to eq 200

    expect(response.headers["ETag"].present?).to be true
    expect(response.headers["Last-Modified"]).to eq(model_type.car_model.updated_at.httpdate)

    data = JSON.parse(response.body)
    expect(data["car_model"]["name"]).to eq("serie_1")
    expect(data["car_model"]["model_types"].size).to eq(1)

    mt = data["car_model"]["model_types"].first
    expect(mt["name"]).to eq("bmw_116i")
    expect(mt["slug"]).to eq("bmw_116i")
    expect(mt["total_price"]).to eq(180015)
  end
end
