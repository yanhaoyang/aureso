FactoryGirl.define do
  factory :model_type do
    car_model
    name "bmw_116i"
    slug "bmw_116i"
    base_price 180000
  end
end
