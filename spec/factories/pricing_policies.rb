FactoryGirl.define do
  factory :pricing_policy do
    trait :fixed do
      name "Fixed"
    end

    trait :flexible do
      name "Flexible"
    end

    trait :prestige do
      name "Prestige"
    end
  end
end
