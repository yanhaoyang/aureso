FactoryGirl.define do
  factory :organization do
    name "name"
    public_name "public name"
    association :type, factory: :organization_type
    association :pricing_policy, factory: [ :pricing_policy, :fixed ]
  end
end
