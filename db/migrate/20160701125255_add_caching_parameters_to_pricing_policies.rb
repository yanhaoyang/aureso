class AddCachingParametersToPricingPolicies < ActiveRecord::Migration
  def change
    add_column :pricing_policies, :http_expires, :datetime
    add_column :pricing_policies, :http_last_modified, :datetime
    add_column :pricing_policies, :http_etag, :string, limit: 128
  end
end
