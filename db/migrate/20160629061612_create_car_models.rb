class CreateCarModels < ActiveRecord::Migration
  def change
    create_table :car_models do |t|
      t.integer :organization_id, null: false
      t.string :name, null: false, limit: 100
      t.string :slug, null: false, limit: 100
      t.timestamps null: false
    end

    add_index :car_models, :organization_id
    add_index :car_models, :slug
  end
end
