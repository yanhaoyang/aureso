class AddUniqIndexToSlugs < ActiveRecord::Migration
  def up
    remove_index :car_models, :slug
    remove_index :model_types, :slug

    add_index :car_models, :slug, unique: true
    add_index :model_types, :slug, unique: true
  end

  def down
    remove_index :car_models, :slug
    remove_index :model_types, :slug

    add_index :car_models, :slug
    add_index :model_types, :slug
  end
end
