class CreateOrganizations < ActiveRecord::Migration
  def change
    create_table :organizations do |t|
      t.integer :organization_type_id, null: false
      t.string :name, null: false, limit: 100
      t.string :public_name, null: false, limit: 100
      t.integer :pricing_policy_id, null: false
      t.timestamps null: false
    end

    add_index :organizations, :organization_type_id
    add_index :organizations, :name
    add_index :organizations, :pricing_policy_id
  end
end
