class CreateModelTypes < ActiveRecord::Migration
  def change
    create_table :model_types do |t|
      t.integer :car_model_id, null: false
      t.string :name, null: false, limit: 100
      t.string :slug, null: false, limit: 100
      t.string :code, limit: 32
      t.decimal :base_price, null: false, precision: 12, scale: 2
      t.timestamps null: false
    end

    add_index :model_types, :car_model_id
    add_index :model_types, :slug
  end
end
