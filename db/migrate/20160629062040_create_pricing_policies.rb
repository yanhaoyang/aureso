class CreatePricingPolicies < ActiveRecord::Migration
  def change
    create_table :pricing_policies do |t|
      t.string :name, null: false, limit: 16
      t.decimal :margin, precision: 12, scale: 2
      t.timestamps null: false
    end
  end
end
