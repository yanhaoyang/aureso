require 'active_record/fixtures'

ActiveRecord::Base.transaction do
  dir = File.join(Rails.root, 'db', 'seeds')
  Dir.glob(File.join(dir, '*.yml')).each do |f|
    t = File.basename(f, '.yml')
    ActiveRecord::Base.connection.execute("DELETE FROM #{t}")
    puts "Seeding #{t} ..."
    ActiveRecord::FixtureSet.create_fixtures(dir, t)
  end

  if ENV["SAMPLE_DATA"]
    FactoryGirl.create :model_type
    puts "Created sample data"
  end

  puts "Done!"
end
