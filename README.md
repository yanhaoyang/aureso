## Aureso – code exercise

This application is built for [Aureso's code exercise](http://hr-manger.herokuapp.com/aureso_welcome/012a082ef0da72522e639c02ad30fb41).

### Architecture

#### Framework

It is implemented as a Rails application. If it does not render web pages at all,
[Rails::API](https://github.com/rails-api/rails-api) or [Rails 5](http://edgeguides.rubyonrails.org/api_app.html)
should be better choice.

#### Models and Database

It uses MySQL as data storage.

Organization is a top-level model. CarModel belongs to an organization, and ModelType also belongs to
an organization through CarModel.

OrganizationType and PricingPolicy are auxiliary models.

#### External API

PricingPolicies module is implemented to access external API. It accesses external
API with [REST Client](https://github.com/rest-client/rest-client). To improve
performance, consider to use a curl-based library.

[Nokogiri](https://github.com/sparklemotion/nokogiri) is used to parse HTML/XML.

It raises exceptions when the external API is unaccessible. It is up to client code
to decide how to handle these exceptions according to business requirements.
If a stale margin is acceptable, the cached value can be used.

#### API

The controllers are designed to be RESTful. The URI may look like this:

* /api/v1/models/:model\_slug/model\_types/:model\_type\_slug/update\_price

The URI is rewritten in routes file as the format defined in requirements:

* /api/v1/models/:model\_slug/model\_types\_price/:model\_type\_slug

#### Authentication

API key is issued per organization as JSON Web Token. The token expires in 2 hours.

The API key can be generated from command line only. In practice, users should
be able to generate API keys and see them, e.g. from an account center.

### Usage

A Docker Compose file has been included. To boot up the application:

    $ bin/run_in_docker

The script will start docker containers, setup database, and start the application.

To generate an API key:

    $ docker exec -it aureso_aureso_1 bash -c 'bin/rails r "puts Organization.first.api_key"'

Then try the API:

    $ curl -H "api-key: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..." \
    $ http://localhost:3000/api/v1/models/serie_1/model_types

And the response:

    {"car_model":{"name":"serie_1","model_types":[{"name":"bmw_116i","slug":"bmw_116i","total_price":180015}]}}

And try to update base price:

    $ curl -H "api-key: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..." \
    $ -X POST -d "base_price=88888" \
    $ http://localhost:3000/api/v1/models/serie_1/model_types_price/bmw_116i

The response:

    {"model_type":{"name":"bmw_116i","base_price":88888,"total_price":88903}}
