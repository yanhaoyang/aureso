class ModelType < ActiveRecord::Base
  belongs_to :car_model, touch: true

  validates_presence_of :car_model, :name, :slug, :base_price
  validates_numericality_of :base_price, greater_than: 0
  validates_uniqueness_of :slug, scope: :car_model

  def total_price
    base_price + car_model.organization.pricing_policy.margin
  end
end
