class CarModel < ActiveRecord::Base
  belongs_to :organization
  has_many :model_types

  validates_presence_of :organization, :name, :slug
  validates_uniqueness_of :slug, scope: :organization
end
