class Organization < ActiveRecord::Base
  belongs_to :type, class_name: "OrganizationType", foreign_key: :organization_type_id
  belongs_to :pricing_policy
  has_many :car_models

  validates_presence_of :type, :name, :public_name, :pricing_policy

  def self.current=(organization)
    Thread.current[:current_organization] = organization
  end

  def self.current
    Thread.current[:current_organization]
  end

  def api_key
    return unless id

    hmac_secret = Rails.application.secrets.jwt_secret
    exp = Time.now.to_i + 4 * 3600
    payload = {
      exp: exp,
      data: {
        organization_id: id,
      },
    }
    JWT.encode payload, hmac_secret, 'HS256'
  end
end
