class ModelWithModelTypesSerializer < ActiveModel::Serializer
  attributes :name
  has_many :model_types, serializer: ModelTypeForModelSerializer
end
