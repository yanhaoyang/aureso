class ModelTypeForModelSerializer < ActiveModel::Serializer
  attributes :name, :slug, :total_price

  def total_price
    object.total_price.to_i
  end
end
