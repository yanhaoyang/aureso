class ModelTypeSerializer < ActiveModel::Serializer
  attributes :name, :base_price, :total_price

  def base_price
    object.base_price.to_i
  end

  def total_price
    object.total_price.to_i
  end
end
