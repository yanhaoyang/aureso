require "pricing_policies"

module Api
  module V1
    class CarModelsController < ApplicationController
      def model_types
        organization = Organization.current
        car_model = organization.car_models.where(slug: params[:model_slug]).
          includes(:model_types).first

        if car_model && stale?(car_model)
          begin
            PricingPolicies.find(organization.pricing_policy).refresh
          rescue
            Rails.logger.error("Failed to refresh pricing_policy: #{$!}")
            render nothing: true, status: :service_unavailable
            return
          end

          serializer = ModelWithModelTypesSerializer.new(car_model)
          adapter = ActiveModelSerializers::Adapter.create(serializer, adapter: :json)
          render json: adapter
        else
          render nothing: true, status: :not_found
        end
      end
    end
  end
end
