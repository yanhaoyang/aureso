require "pricing_policies"

module Api
  module V1
    class ModelTypesController < ApplicationController
      def update_price
        organization = Organization.current
        car_model = organization.car_models.where(slug: params[:model_slug]).first
        unless car_model
          render nothing: true, status: :not_found
          return
        end

        model_type = car_model.model_types.where(slug: params[:model_type_slug]).first
        unless car_model
          render nothing: true, status: :not_found
          return
        end

        begin
          PricingPolicies.find(organization.pricing_policy).refresh
        rescue
          Rails.logger.error("Failed to refresh pricing_policy: #{$!}")
          render nothing: true, status: :service_unavailable
          return
        end

        model_type.base_price = params[:base_price]
        if model_type.save
          serializer = ModelTypeSerializer.new(model_type)
          adapter = ActiveModelSerializers::Adapter.create(serializer, adapter: :json)
          render json: adapter
        else
          render nothing: true, status: :unprocessable_entity
        end
      end
    end
  end
end
