class ApplicationController < ActionController::Base
  before_action :authenticate_api

  private

  def authenticate_api
    hmac_secret = Rails.application.secrets.jwt_secret
    token = request.headers["api-key"]
    result = false
    begin
      decoded_token = JWT.decode token, hmac_secret, true, { :algorithm => 'HS256' }
      if (organization = Organization.find(decoded_token.first["data"]["organization_id"]))
        Organization.current = organization
        result = true
      end
    rescue JWT::ExpiredSignature
      # do nothing
    rescue JWT::DecodeError
      # do nothing
    end

    render nothing: true, status: :unauthorized unless result
  end
end
