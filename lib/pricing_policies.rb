require 'pricing_policies/base'
require 'pricing_policies/fixed'
require 'pricing_policies/flexible'
require 'pricing_policies/prestige'

module PricingPolicies
  class NotImplementedError < StandardError; end

  # Find a pricing policy implementation by model
  def self.find(obj)
    klass = const_get(obj.name)
    klass.new(obj)
  end
end
