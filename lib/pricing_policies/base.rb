require 'rest-client'
require 'nokogiri'

module PricingPolicies
  class Base
    def initialize(obj)
      @obj = obj
    end

    # Refresh the margin and update caching headers when necessary
    # Raises exception when the API is inaccessible
    def refresh
      if @obj.http_expires && @obj.http_expires > Time.now
        # The margin is still valid. Do nothing.
      else
        headers = {}
        if @obj.http_last_modified
          headers["If-Modified-Since"] = @obj.http_last_modified.httpdate
        end
        if @obj.http_etag
          headers["If-None-Match"] = @obj.http_etag
        end

        begin
          response = RestClient::Request.execute(method: :get, url: api_uri, headers: headers)
        rescue RestClient::NotModified
          return # nothing changed
        end

        # We already get a response if RestClient does not raise any exception
        @obj.http_expires = Time.parse(response.headers[:expires]) if response.headers[:expires]
        @obj.http_last_modified = Time.parse(response.headers[:last_modified]) if response.headers[:last_modified]
        @obj.http_etag = response.headers[:etag] if response.headers[:etag]

        @obj.margin = calculate_margin(response)
      end
    end

    private

    def api_uri
      fail NotImplementedError, "#{self.class.name}#__method__"
    end

    def calculate_margin
      fail NotImplementedError, "#{self.class.name}#__method__"
    end
  end
end
