require 'pricing_policies/base'

module PricingPolicies
  class Prestige < Base
    private

    def api_uri
      "http://www.yourlocalguardian.co.uk/sport/rugby/rss/".freeze
    end

    def calculate_margin(response)
      doc = Nokogiri::XML(response.body)
      doc.xpath("//pubDate").size
    end
  end
end
