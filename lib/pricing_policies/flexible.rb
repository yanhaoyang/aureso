require 'pricing_policies/base'

module PricingPolicies
  class Flexible < Base
    private

    def api_uri
      "http://www.reuters.com/".freeze
    end

    def calculate_margin(response)
      doc = Nokogiri::HTML(response.body)
      doc.xpath("//script").remove if doc.xpath("//script").size > 0
      doc.xpath("//style").remove if doc.xpath("//style").size > 0
      doc.content.scan("a").size
    end
  end
end
