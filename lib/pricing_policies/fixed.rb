require 'pricing_policies/base'

module PricingPolicies
  class Fixed < Base
    private

    def api_uri
      "https://developer.github.com/v3/".freeze
    end

    def calculate_margin(response)
      doc = Nokogiri::HTML(response.body)
      doc.xpath("//script").remove if doc.xpath("//script").size > 0
      doc.xpath("//style").remove if doc.xpath("//style").size > 0
      doc.content.scan(/\bstatus\b/i).size
    end
  end
end
