Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :models, controller: :car_models, param: :slug, only: [] do
        get :model_types
      end
      post "models/:model_slug/model_types_price/:model_type_slug",
        to: "model_types#update_price", as: "update_model_types_price"
    end
  end
end
